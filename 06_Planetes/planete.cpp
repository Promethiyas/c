#include "planete.h"
#include<QTextStream>
#include<QString>

Planete::Planete(QString nom, int diam, double dist):m_nom(nom), m_diametre(diam), m_distance(dist)
{

}

QString Planete::toString()
{
    QString res;
    QTextStream buf(&res);
    buf<<"Planete "<<m_nom<<" - diam = "<<m_diametre<<" km";
    buf<<" - dist = "<<m_distance<<" Mkm";
    return res;
}

double Planete::distanceEntre(Planete p)
{
    return std::abs(m_distance - p.m_distance);
}

double Planete::distance() const
{
    return m_distance;
}
