#ifndef PLANETE_H
#define PLANETE_H

#include <QString>

class Planete
{
private:
    QString m_nom;
    int m_diametre;
    double m_distance;
public:
    Planete(QString nom, int diam, double dist);
    QString toString();
    double distanceEntre(Planete p);
    double distance() const;
};

#endif // PLANETE_H
